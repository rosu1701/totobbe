//------------------------------------------------------------------------------
// Lab3.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "Lab3.h"
#include "PowerSourceAdapters.h"
#include <iostream>


/**
 * Main program
 */

int main() {
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}