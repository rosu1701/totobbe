#pragma once

#include "Talker.h"


class SoundProducer
{
private:
	std::string soundName;
public:
	// Constructor
	SoundProducer(std::string nameOfSound) { soundName = nameOfSound; }
	//
	virtual ~SoundProducer() { std::cout << "SoundProducer destructor called! " << std::endl; }
	// printers
	virtual void makeSound() {};
	// getters
	std::string getSoundName() { return soundName; }
	
};


class Whisperer : public SoundProducer
{
private:
public:
	Whisperer() :SoundProducer("Whisperer") {};
	void makeSound() override { std::cout << getSoundName() << ": Ssch, hush, hush" << std::endl; }
};

class Shouter : public SoundProducer
{
private:
public:
	Shouter() :SoundProducer("Shouter") {};
	void makeSound() override { std::cout << getSoundName() << ": WOW YEEH!!" << std::endl; }
};

class Uppgift1 : public Talker
{
private:
	SoundProducer *soundP;

public:
	// constructor
	Uppgift1() {};
	// deconstructor
	~Uppgift1() { std::cout << "Uppgift1 destructor called" << std::endl;  delete soundP; }
	// setters
	void setSoundProducer(SoundProducer *sp) {delete soundP; soundP = sp; }
	void saySomething() override { soundP->makeSound(); } ;
};