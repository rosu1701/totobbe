#pragma once

// interface
class Talker
{
public :
	// pure virtual function
	virtual void saySomething() = 0;

private:
};