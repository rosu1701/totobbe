//------------------------------------------------------------------------------
// Lab1.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab1.h"
#include <iostream>
#include "Uppgift1.h"
#include "C:\Users\Master\dev\tools\include\memstat.hpp"
#include "C:\Users\Master\dev\tools\include\mempool.hpp"

/**
 * Main program
 */
int main() {
	std::cout << getAssignmentInfo() << std::endl;



		Uppgift1 uppg1;

		uppg1.setSoundProducer(new Whisperer());
		uppg1.saySomething();
		uppg1.setSoundProducer(new Shouter());
		uppg1.saySomething();



	std::cin.get();

    return 0;
}


