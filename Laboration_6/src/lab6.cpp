//------------------------------------------------------------------------------
// Lab6.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab6.h"
#include <iostream>

/**
 * Main program
 */
int main() {
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}