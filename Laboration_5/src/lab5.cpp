//------------------------------------------------------------------------------
// Lab5.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "Lab5.h"
#include "HanoiEngine.h"
#include <iostream>

/**
 * Main program
 */
int main() {
    std::cout << getAssignmentInfo() << std::endl;
    //HanoiEngine h = HanoiEngine(3);
    //h.show();
    return 0;
}