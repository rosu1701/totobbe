


//
// Created by Master on 2019-01-25.
//
#pragma once

#include <vector>
#include "Ingredient.hpp"
#include <algorithm>
#include <string>

// Base class for all recipes
class BakingRecepy
{
protected:
    std::string name;
    std::vector<Ingredient> needIng;
public:
    BakingRecepy() { std::cout << "BakingRecepy constructor" << std::endl; }
    explicit BakingRecepy(std::string aName) : name(std::move(aName)) {};

    // Getters
    const std::string &getName() const { return name; };

    bool isBakeable(std::vector<Ingredient> &availableIng);
    virtual void bakeIt() = 0;
};

class PizzaRecepy : public BakingRecepy
{

public:
    explicit PizzaRecepy(std::string aName) : BakingRecepy(aName) {
        needIng.push_back((Ingredient)("oil"));
        needIng.push_back((Ingredient)("yeast"));
        needIng.push_back((Ingredient)("wheat-flour"));
        needIng.push_back((Ingredient)("salt"));
    };

    void bakeIt() override;
};

class SconesRecepy : public BakingRecepy
{

public:
    explicit SconesRecepy(std::string aName) : BakingRecepy(aName) {
        needIng.push_back((Ingredient)("oil"));
        needIng.push_back((Ingredient)("baking-powder"));
        needIng.push_back((Ingredient)("wheat-flour"));
        needIng.push_back((Ingredient)("salt"));
        needIng.push_back((Ingredient)("sugar"));
        needIng.push_back((Ingredient)("milk"));
    };

    void bakeIt() override;
};

class KladdkakaRecepy : public BakingRecepy
{

public:
    explicit KladdkakaRecepy(std::string aName) : BakingRecepy(aName) {
        needIng.push_back((Ingredient)("egg"));
        needIng.push_back((Ingredient)("baking-powder"));
        needIng.push_back((Ingredient)("wheat-flour"));
        needIng.push_back((Ingredient)("salt"));
        needIng.push_back((Ingredient)("sugar"));
        needIng.push_back((Ingredient)("marge"));
        needIng.push_back((Ingredient)("cocoa-powder"));
    };

    void bakeIt() override;
};

class SugarcakeRecepy : public BakingRecepy
{

public:
    explicit SugarcakeRecepy(std::string aName) : BakingRecepy(aName) {
        needIng.push_back((Ingredient)("egg"));
        needIng.push_back((Ingredient)("baking-powder"));
        needIng.push_back((Ingredient)("wheat-flour"));
        needIng.push_back((Ingredient)("lemon"));
        needIng.push_back((Ingredient)("sugar"));
        needIng.push_back((Ingredient)("marge"));

    };

    void bakeIt() override;
};

// For testing purposes
class PoopCake : public BakingRecepy
{

public:
    explicit PoopCake(std::string aName) : BakingRecepy(aName) {
            needIng.push_back((Ingredient)("poop"));
            needIng.push_back((Ingredient)("egg"));
            needIng.push_back((Ingredient)("baking-powder"));
            needIng.push_back((Ingredient)("wheat-flower"));
            needIng.push_back((Ingredient)("lemon"));
            needIng.push_back((Ingredient)("sugar"));

            needIng.push_back((Ingredient)("marge"));
    };

    void bakeIt() override;
};