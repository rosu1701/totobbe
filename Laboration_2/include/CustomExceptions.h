//
// Created by Master on 2019-01-29.
//

#pragma once

#include <exception>

struct NoBakingException : public std::exception
{
   const char * what() const throw()
   {
        return("No More recepes!");
   }

};