//
// Created by Master on 2019-01-28.
//

#pragma once
#include "Recipes.h"
#include "CustomExceptions.h"
#include <fstream>
#include <memory>

class BakingRecepyManager
{
private:
    std::vector<std::shared_ptr<BakingRecepy>> recipes;
    int recipeInc = 0;
public:
    BakingRecepyManager(std::string fileName);
    ~BakingRecepyManager() {}

    bool hasAnotherRecepy();
    std::shared_ptr<BakingRecepy> getNextBakingRecepy();

};

class Bakery
{
private:
    BakingRecepyManager brm;
public:
    explicit Bakery(std::string fileName) : brm(std::move(fileName)) {}

    BakingRecepyManager& getBRM() {return brm;}
};