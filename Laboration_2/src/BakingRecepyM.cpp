//
// Created by Master on 2019-01-28.
//

#include "BakingRecepyM.h"

BakingRecepyManager::BakingRecepyManager(std::string fileName)
{
    // vector for storing available ingredients
    std::vector<Ingredient> ingredients;
    // vector for all recepes
    std::vector<std::shared_ptr<BakingRecepy>> availableRecepes;
    // fill recepes with all recepes
    availableRecepes.push_back(std::make_shared<PoopCake>("poop"));
    availableRecepes.push_back(std::make_shared<SugarcakeRecepy> ("sugar"));
    availableRecepes.push_back(std::make_shared<PizzaRecepy> ("pizza"));
    availableRecepes.push_back(std::make_shared<SconesRecepy> ("scones"));
    availableRecepes.push_back(std::make_shared<KladdkakaRecepy>("kladdkaka"));
    // add a recipe that cannot be cooked with the current ingredients(For testing purposes!)


    std::ifstream infile(fileName);
    if (!infile.is_open())
        std::cout << "unable to open file" << std::endl;


    std::string line;
    while (getline(infile, line)) {
        // Ingredient is a class provided for solving the assignment and
        // you find it under ../_CodeBase/Bakery/
        ingredients.push_back((Ingredient)(line));
    }



    // close file
    infile.close();


    // check which recipes can be baked with current ingredients
    for(auto &v : availableRecepes)
    {
        // check if the recepy can be baked with current ingredients
       if(!v->isBakeable(ingredients))
       {
           // print message if it could not
           std::cout << v->getName() <<" could not be baked ´with the current ingredients" << std::endl << std::endl;
       }
      else
          // move bakeable recepes to the recepes vector
           recipes.push_back(std::move(v));
    }

    /*for(auto &v : recipes)
    {
        v->bakeIt();
    }
    */


}

bool BakingRecepyManager::hasAnotherRecepy()
{
    if(recipeInc >= (recipes.size()))
        return false;

    return true;
}


std::shared_ptr<BakingRecepy> BakingRecepyManager::getNextBakingRecepy()
{
    // if there is no more recipes throw exception
    if(!hasAnotherRecepy())
        throw NoBakingException();

    // increment variable for checking index
    // temp int for index
    int temp = recipeInc;
    recipeInc++;


    return recipes.at(temp);

}

