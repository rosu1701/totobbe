//
// Created by Master on 2019-01-25.
//

#include "Recipes.h"

bool BakingRecepy::isBakeable(std::vector<Ingredient> &availableIng)
{
// check every item of needed ingredients against available ingredients
    for(auto &v : needIng)
    {

// if the available ingredients contains the needed one: do nothing. if not the recipe is un bakeable
        if(std::find(availableIng.begin(),availableIng.end(), v) != availableIng.end())
        {}
        else
            return false;
    }

// if every needed ingredient was found, return true
    return true;
}



void PizzaRecepy::bakeIt()
{


    std::cout << "HOW TO BAKE A PIZZA!" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    std::cout << "INGREDIENTS NEEDED!" << std::endl;
    for(auto &v : needIng)
    {std::cout << v << std:: endl;}
    std::cout << "-----------------------------------" << std::endl;

    std::cout << "First you blend the yeast with lukewarm water" << std::endl
    << "then you pour in the flower, and the oil. Season with a pinch of salt" << std::endl
    << "Now you got your self a paste for the dough!" << std::endl << std::endl << std::endl;;
}

void SconesRecepy::bakeIt()
{



    std::cout << "HOW TO BAKE SCONES!" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    std::cout << "INGREDIENTS NEEDED!" << std::endl;
    for(auto &v : needIng)
    {std::cout << v << std:: endl;}
    std::cout << "-----------------------------------" << std::endl;

    std::cout << "First you blend the yeast with lukewarm milk" << std::endl
              << "then you pour in the flower,the baking-powder and the oil. Season with a pinch of salt and a dl of sugar" << std::endl
              << "Separate the dough into nice chunks and put them in the oven" << std::endl << std::endl << std::endl;;
}

void KladdkakaRecepy::bakeIt()
{
    std::cout << "HOW TO BAKE A KLADDKAKA!" << std::endl;

    std::cout << "-----------------------------------" << std::endl;
    std::cout << "INGREDIENTS NEEDED!" << std::endl;
    for(auto &v : needIng)
    {std::cout << v << std:: endl;}
    std::cout << "-----------------------------------" << std::endl;

    std::cout << "Heat the eggs. cocoa-powder, marge and sugar until it has combined into a nice mess" << std::endl
              << "then you pour in the flower and the baking-powder. Season with a pinch of salt" << std::endl
              << "Pour the batter into a baking pan" << std:: endl << std::endl << std::endl;;
}

void SugarcakeRecepy::bakeIt()
{
    std::cout << "HOW TO BAKE A SUGARCAKE!" << std::endl;

    std::cout << "-----------------------------------" << std::endl;
    std::cout << "INGREDIENTS NEEDED!" << std::endl;
    for(auto &v : needIng)
    {std::cout << v << std:: endl;}
    std::cout << "-----------------------------------" << std::endl;

    std::cout << "Heat the marge until liquid. Blend the eggs, marge and sugar until it has combined into a nice mess" << std::endl
              << "then you pour in the flower and the baking-powder. Season with a squeese of lemon" << std::endl
              << "Pour the batter into a baking pan" << std:: endl << std::endl << std::endl;
}

void PoopCake::bakeIt()
{
    std::cout << "HOW TO BAKE A POOPCAKE!" << std::endl;

    std::cout << "-----------------------------------" << std::endl;
    std::cout << "INGREDIENTS NEEDED!" << std::endl;
    for(auto &v : needIng)
    {std::cout << v << std:: endl;}
    std::cout << "-----------------------------------" << std::endl;

    std::cout << "Heat the marge until liquid. Blend the eggs, poop, marge and sugar until it has combined into a nice mess" << std::endl
              << "then you pour in the flower and the baking-powder. Season with a squeese of lemon" << std::endl
              << "Pour the batter into a baking pan" << std:: endl << std::endl << std::endl;
}