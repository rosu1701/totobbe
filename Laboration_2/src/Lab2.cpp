//------------------------------------------------------------------------------
// Lab2.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "Lab2.h"
#include "Ingredient.hpp"
#include <iostream>
#include <fstream>
#include "BakingRecepyM.h"

/**
 * Main program
 *
 * Demonstrates use of bakery
 */
int main() {
    std::cout << getAssignmentInfo() << std::endl;


    // creat Bakery object
    Bakery test("../_Resources/pantry.dat");

    try {

        // make call to getNextBakingRecepy to many times for testing purposes, will throw exception if there is no more recipes!
        test.getBRM().getNextBakingRecepy()->bakeIt();
        test.getBRM().getNextBakingRecepy()->bakeIt();
        test.getBRM().getNextBakingRecepy()->bakeIt();
        test.getBRM().getNextBakingRecepy()->bakeIt();
        test.getBRM().getNextBakingRecepy()->bakeIt();
        test.getBRM().getNextBakingRecepy()->bakeIt();

    }
    catch(NoBakingException nBE)
    {
        // No more recipes!
        std::cout << nBE.what() << std::endl;
    }



    return 0;
}

